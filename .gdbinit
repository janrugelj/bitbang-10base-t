target remote :3333
# target remote :2331


define ml
make
load
end

define reset
monitor reset halt
end


# define reset
# monitor reset
# end

# define halt
# monitor halt
# end


define rc
reset
continue
end


python
def sb(n):
    for i in range(0,32):
        if n & 1<<i:
            print(i)
def cb(n):
    for i in range(0,32):
        if not (n & 1<<i):
            print(i)
end

define sb
python sb($arg0)
end

define cb
python cb($arg0)
end


alias -a m = monitor

