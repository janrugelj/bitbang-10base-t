## Cross-compilation commands
CC      = arm-none-eabi-gcc
LD      = arm-none-eabi-gcc
AR      = arm-none-eabi-ar
AS      = arm-none-eabi-as
OBJCOPY = arm-none-eabi-objcopy
OBJDUMP = arm-none-eabi-objdump
SIZE    = arm-none-eabi-size --format=SysV -x



## clock info ##
F_CPU = 80000000
CFLAGS += -DF_CPU=$(F_CPU)

## core and hardware info ##
CORE_FAMILY = CORTEX_M
CORE = CORE_CM0PLUS
MCU_FAMILY = STM32G0xx
MCU_NAME = STM32G071xx
CFLAGS += -D$(CORE_FAMILY) -D$(CORE) -D$(MCU_FAMILY) -D$(MCU_NAME)

## linker script and startup file location ##
LINKSCRIPT = ./stm32g071/STM32G071RBTx_FLASH.ld
STARTUP = startup_stm32g071xx.s





## add main headers and sources ##
INC = -I./
CSRC = main.c delay.c gpio_10baseT.c master_spi_10baseT.c uart.c udp_ipv4_mac_eth.c xprintf.c


## add CMSIS headers, c sources and assembly sources ##
# locate CMSIS folder and startup file
CMSIS = ./stm32g071/CMSIS
CMSIS_STARTUP = $(CMSIS)/Device/ST/$(MCU_FAMILY)/Source/Templates
# who needs hal?
# HAL       = ./stm32g071/STM32G0xx_HAL_Driver
# HAL_SRC   = $(HAL)/Src
# CMSIS_DSP = $(CMSIS)/DSP

INC += -I$(CMSIS)/Include/
INC += -I$(CMSIS)/Device/ST/$(MCU_FAMILY)/Include/
# INC += -I$(CMSIS_DSP)/Include
# INC += -I$(HAL)/Inc

CMSIS_CSRC := $(CMSIS_STARTUP)/system_stm32g0xx.c #$(wildcard $(CMSIS_STARTUP)/*.c)
# CMSIS_CSRC += $(wildcard $(CMSIS_DSP)/Source/*/*.c)
CSRC += $(CMSIS_CSRC)

CMSIS_ASRC = $(CMSIS_STARTUP)/gcc/$(STARTUP)
ASRC += $(CMSIS_ASRC) gpio_10baseT_asm.s



# define all object files (from c and assembly source)
OBJS = $(CSRC:.c=.o)
OBJS += $(ASRC:.s=.o)

# define all dependency files (from object files)
DEPS := $(OBJS:.o=.d)



## compiler flags ##

# enable printing debug messages
# it prints debug messages in interrupt so it is slower and will
# cause a disconnect if sending packets >200bytes (because NLP pulse will not
# be sent soon enough)
# CFLAGS += -DDEBUG

# optimization, c standard
CFLAGS += -c -std=c99  -O2

# debug info
FLAGS_DEBUG += -gdwarf-4 -g3 -ggdb
CFLAGS += $(FLAGS_DEBUG)

# architecture specific
FLAGS_ARCH = -mcpu=cortex-m0plus -mthumb -mlittle-endian
CFLAGS += $(FLAGS_ARCH)

# etc
CFLAGS += -Wall -ffunction-sections -fdata-sections -fno-builtin
CFLAGS += -Wno-unused-function -ffreestanding -fno-common

# generate dependency files (*.d)
CFLAGS += -MMD -MP
# print dependency files
# $(info $$DEPS is [${DEPS}])

# append include paths
CFLAGS += $(INC)



## linker flags ##
LFLAGS  = -T$(LINKSCRIPT)
LFLAGS += $(FLAGS_DEBUG) $(FLAGS_ARCH)
LFLAGS += -Wl,--gc-sections -Wl,-Map=main.map



# targets
all: size

size: main.elf
	$(SIZE) $<

main.elf: main.o $(OBJS)
	$(LD) $(LFLAGS) $^ -o $@

%.hex: %.elf
	$(OBJCOPY) --strip-unneeded -O ihex $< $@

%.bin: %.elf
	$(OBJCOPY) --strip-unneeded -O binary $< $@

clean:
	-rm -f $(OBJS) $(DEPS) main.o main.lst main.elf main.hex main.map main.bin main.list

# include all dependency files so make knows which files to
# rebuild if headers change
#
# this include basically inserts dependencies of all source files, eg:
# main.o : main.c foo.h bar.h
-include $(DEPS)

# targets which are not files
.PHONY: all size clean

