
This repo contains software implementation of Full-duplex Ethernet 10BASE-T
protocol with stm32g071 microcontroller.

Only required external components are RS-485 receiver, RS-485 transmitter,
mangetics and 100ohm resistor.

There is also a simpler implementation of TX only with just 4 mosfet
transistors, 2 capacitors and magnetics.
It bitbangs TX signal with two GPIO pins.



## TX and RX Full-Duplex 10BASE-T with DMA, SPI and IRQ

### operation:

`SPI` is running at 40 MHz. (microcontroller overclocked to 80 MHz)

Two `DMAs` are used. `First DMA` is constantly sending data from buffer in ram
to SPI.

When we want to send data we encode it in Manchester encoding and write it to
buffer accessed by `first DMA`.
Data is shifted out through `MOSI PA7` pin.
To enable RS485 transmitters output we must set `DE` (data enable) pin.
This is done with `GPIO PA1` connected to timer `TIM2 CH2`, so it can be
automatically deactivated when frame is sent.

To detect incoming frames a pin change interrupt is used on `PA0`.
Same signal is also connected to `MISO PA6`.
After detecting frame `second DMA` is activated. `Second DMA` copies data from
`SPI` to another buffer in ram.
End of frame is detected by constantly monitoring `SPI->DR` register, so it
consumes 100% cpu time.

Manchester encoding/decoding is done in software.

Sending frame requires 0 cpu time so it can start receiving frame while
transmitting.
(Manchester encoding/decoding takes more time than sending/receiving frame.)


### pin assignment:
- PA1 as TIM2 CH2 (for TX)
- PA7 as SPI1 MOSI (for TX)
- PA0 as pin change interrupt (for RX)
- PA6 as SPI MISO (for RX)

### required external components:
- 1x ISL3295EFHZ-T RS-485 tramsmitter 20 Mbps
- 1x MAX3280EAUK+T RS-485 receiver 52Mbps (must be at least 20 Mbps)
- 1x 100 ohm resistor
- 1x magnetics

### schematics:

```
+-----------------------------------------------------------------------------+
|   STM32G071                                                                 |
|   +-------------------+         RS485 Transmitter 20Mbps                    |
|   |                   |         +-----------------+                         |
|   |     PA1/TIM2 CH2  +---------+ DE            A +------- to TX+ magnetics |
|   | TX                |         |                 |                         |
|   |     PA7/SPI1 MOSI +---------+ DI            B +------- to TX- magnetics |
|   |                   |         +-----------------+                         |
|   |                   |                                                     |
|   |                   |         RS485 Receiver   20Mbps                     |
|   |                   |         +-----------------+                         |
|   |     PA0/IRQ       +----+----+ RO            A +--+---- to RX+ magnetics |
|   | RX                |    |    |                 |  R 100ohm               |
|   |     PA6/SPI1 MISO +----+    |               B +--+---- to RX- magnetics |
|   |                   |         +-----------------+                         |
|   +-------------------+                                                     |
+-----------------------------------------------------------------------------+
```


## TX only 10BASE-T with GPIO

### operation:

Frame is converted ("compiled") into assembly instructions setting and
resetting pins `PB0` and `PB1` and stored in ram. It is then executed from ram.

### pin assignment:
- PB0 as DP (data plus)
- PB1 as DM (data minus)

### required external components:
- 2x FDV301N -- N-channel Mosfet (gate capacitance 9.5 pF)
- 2x FDV302P -- P-chennel Mosfet (gate capacitance 11 pF)
- 2x ceramic capacitor 10 nF
- magnetics

### schematics:

```
+-----------------------------------------------------------------------+
|     STM32G071                                                         |
|     +-------------------+                                             |
|     |                   |          VCC 3V3                            |
|     |                   |             |                               |
|     |                   |             S                               |
|     |                   |     +--G(P-mosfet)                          |
|     |                   |     |       D        10nF                   |
|     |               PB0 +-----+       +---------C--- to TX+ magnetics |
|     |                   |     |       D                               |
|     |                   |     +--G(N-mosfet)                          |
|     |                   |             S                               |
|     |                   |             |                               |
|     |                   |            GND                              |
|     | TX                |                                             |
|     |                   |                                             |
|     |                   |          VCC 3V3                            |
|     |                   |             |                               |
|     |                   |             S                               |
|     |                   |     +--G(P-mosfet)                          |
|     |                   |     |       D        10nF                   |
|     |               PB1 +-----+       +---------C--- to TX- magnetics |
|     |                   |     |       D                               |
|     |                   |     +--G(N-mosfet)                          |
|     |                   |             S                               |
|     |                   |             |                               |
|     |                   |            GND                              |
|     +-------------------+                                             |
+-----------------------------------------------------------------------+
```
