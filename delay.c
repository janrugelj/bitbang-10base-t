
#include "delay.h"

#pragma GCC push_options
#pragma GCC optimize ("O3")
void __attribute__ ((noinline)) delay (u32 t)
{
	while(t--) __asm__ ("nop");
}
#pragma GCC pop_options

