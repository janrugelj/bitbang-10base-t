
#include "stm32g0xx.h"

#include "mbclib.h"

/* bitbangs 10base_t (transmit only)
 * - PB0 as DP (data plus)
 * - PB1 as DM (data minus)
 *
 * IMPORTANT: main processor should run at exactly 40MHz
 * use init_clock_40MHz(); and edit Makefile F_CPU accordingly
 */



/* declere assembly functions from gpio_10baseT_asm.s
 * we are using hardcoded lengths:
 * - gpio_10baseT_prepare_regs - 32bytes
 * - gpio_10baseT_set_plus - 2bytes
 * - gpio_10baseT_set_minus - 2bytes
 * - gpio_10baseT_set_idle - 2bytes
 * - gpio_10baseT_nop - 2bytes
 * - gpio_10baseT_end - 2bytes
 */
extern void gpio_10baseT_prepare_regs();
extern void gpio_10baseT_set_plus();
extern void gpio_10baseT_set_minus();
extern void gpio_10baseT_set_idle();
extern void gpio_10baseT_nop();
extern void gpio_10baseT_end();


/* PB0 - DP (data plus) */
/* PB1 - DM (data minus) */
#define ETH_PLUS	((0x2) | (0x1<<16))
#define ETH_MINUS	((0x1) | (0x2<<16))
#define ETH_IDLE	(0x3)

#define ETH_SET_PLUS()		GPIOB->BSRR = ETH_PLUS
#define ETH_SET_MINUS()		GPIOB->BSRR = ETH_MINUS
#define ETH_SET_IDLE()		GPIOB->BSRR = ETH_IDLE


/* opcode/instruction buffer - should be 2*4*8*1530bytes
 * 2bytes for each instruction
 * 4instructions per bit
 * 8bits per byte
 *
 * that is ram 64bytes for 1byte of ethernet frame
 *
 * with 8192bytes of ram we get 128bytes of ethernet
 */
u8 opcode_buff[8192];


/* copies function/opcodes (probably from flash) into opcode buffer in ram
 * - obp - opcode buffer pointer
 * - fp - source function pointer
 * - len_bytes - length of function in bytes
 */
static u8 *append_to_opcode_buff(u8 *obp, u8* fp, u32 len_bytes)
{
	/* substract one because of thumb2 address lsb bit */
	for (u32 i = 0; i < len_bytes; ++i)
		*(obp++) = *( ((u8 *)fp) -1 +i );

	return obp;
}

/*
 * converts one data byte to raw instructions and writes them to buffer.
 * assumes 40MHz clock
 * - obp - opcode buffer pointer
 * - data_byte - byte to convert
 */
static u8 *single_byte_to_opcodes(u8 *obp, u8 data_byte)
{
	/* lsb (least significant bit) first */
	for (u32 i = 0; i < 8; ++i) {
		if (data_byte & (1<<i)) {
			/* manchester encoded 1 */
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_set_minus, 2);
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_nop, 2);
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_set_plus, 2);
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_nop, 2);
		} else {
			/* manchester encoded 0 */
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_set_plus, 2);
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_nop, 2);
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_set_minus, 2);
			obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_nop, 2);
		}
	}

	return obp;
}



/*
 * init GPIOs for bitbang 10baseT
 */
void gpio_10baseT_init()
{
	/* init gpio */
	/*
	 * PB0 output
	 */
	GPIOB->MODER &= ~GPIO_MODER_MODE0;
	GPIOB->MODER |= GPIO_MODER_MODE0_0;
	GPIOB->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED0;
	GPIOB->OSPEEDR |= (0x3 << GPIO_OSPEEDR_OSPEED0_Pos);

	/*
	 * PB1 output
	 */
	GPIOB->MODER &= ~GPIO_MODER_MODE1;
	GPIOB->MODER |= GPIO_MODER_MODE1_0;
	GPIOB->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED1;
	GPIOB->OSPEEDR |= (0x3 << GPIO_OSPEEDR_OSPEED1_Pos);
}

/*
 * converts message to instructions that write to
 * BSSR (bit set reset) register, write them to ram
 * and execute them
 */
void gpio_10baseT_send_frame(u8 *eth_frame, u32 byte_count)
{
	if (byte_count < 72) {
		/* sorry your frame is too short */
		while (1);
	}

	if (byte_count > 1530) {
		/* sorry your frame is too long */
		/* also some buffer overflows probably occurred */
		while (1);
	}

	/* __asm__("bkpt 42"); */
	u8 *obp = opcode_buff;

	obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_prepare_regs, 32);

	for (u32 i = 0; i < byte_count; ++i)
		obp = single_byte_to_opcodes(obp, eth_frame[i]);

	obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_set_idle, 2);

	obp = append_to_opcode_buff(obp, (u8 *)&gpio_10baseT_end, 2);

	/* __asm__("bkpt 42"); */
	((void(*)())opcode_buff + 1)();
}

/*
 * sends normal link pulse
 * this should be called ever 16ms +/-8ms if no other frames are sent
 */
void gpio_10baseT_NLP_pulse()
{
	ETH_SET_PLUS();
	__asm__ ("nop");
	ETH_SET_IDLE();
}



#if 0
/*
 * FUNCTIONS WITH DMA - THIS DOES NOT WORK
 * stm32g071 has GPIOs connected directly to core (through IOPORT bus).
 * DMA only has access to AHB bus and cannot access IOPORT.
 *
 * in might work on some other stm32 micros
 */

/*
 * init dma
 * should be called after gpio_10baseT_init()
 */

/* dma buffer - should be 1*4*8*1530bytes
 * 1byte for each BSRR set/reset
 * 4 BSRRs for one bit
 * 8bits per byte
 */
u8 dma_buff[1234];

void gpio_10baseT_dma_init()
{
	/* init DMA */
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	delay(10);


	/*
	 * setup dma
	 * we want to copy mem-to-periph but there will be no events from
	 * periph so we set MEM2MEM (we also set DIR because it is nicer)
	 * also 32bit source, 32bit dest, increment source
	 */
	DMA1_Channel1->CCR = DMA_CCR_MEM2MEM | DMA_CCR_DIR |
		DMA_CCR_MSIZE_1 | DMA_CCR_PSIZE_1 | DMA_CCR_MINC;

}

/*
 * transfer frame with dma
 */
void gpio_10baseT_dma_send_frame(u8 *eth_frame, u32 byte_count)
{

	/* TODO:
	 * here convert eth_frame to dma_buff
	 */

	/* u32 data[16] = {0x3, (0x2) | (0x1<<16), 0x3}; */
	/* u32 data2[16] = {42,42,42,42,42,42,42,42,}; */

	DMA1_Channel1->CNDTR = byte_count*6*8;
	DMA1_Channel1->CMAR = (u32)&dma_buff;
	DMA1_Channel1->CPAR = (u32)(&GPIOB->BSRR);
	/* DMA1_Channel1->CPAR = (u32)(&GPIOB->ODR); */
	/* DMA1_Channel1->CPAR = (u32)&data2; */

	/* start transfer */
	DMA1_Channel1->CCR |= DMA_CCR_EN;
}
#endif

