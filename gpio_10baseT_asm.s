
.syntax unified
.cpu cortex-m0
.fpu softvfp
.thumb
/* .code 32 */

/*** whole code must be position independent ***/

/* PB0 - D+ (data plus) */
/* PB1 - D- (data minus) */

.type	gpio_10baseT_prepare_regs, %function
.global gpio_10baseT_prepare_regs
gpio_10baseT_prepare_regs:
	/* length of this function is 32bytes */

	/* put 0x50000418 (address of GPIOB->BSRR) into r0 */
	movs r0, 0x50 /* MSB bits */
	lsls r0, 16

	movs r3, 0x4
	orrs r0, r3
	lsls r0, 8

	movs r3, 0x18 /* LSB bits */
	orrs r0, r3


	/* put ((0x2) | (0x1<<16)) into r1 */
	movs r1, 0x1
	lsls r1, 16

	movs r3, 0x2
	orrs r1, r3


	/* put ((0x1) | (0x2<<16)) into r2 */
	movs r2, 0x2
	lsls r2, 16

	movs r3, 0x1
	orrs r2, r3


	/* put  0x3 into r3 */
	movs r3, 0x3



.type	gpio_10baseT_set_plus, %function
.global gpio_10baseT_set_plus
gpio_10baseT_set_plus:
	str r1, [r0]


.type	gpio_10baseT_set_minus, %function
.global gpio_10baseT_set_minus
gpio_10baseT_set_minus:
	str r2, [r0]


.type	gpio_10baseT_set_idle, %function
.global gpio_10baseT_set_idle
gpio_10baseT_set_idle:
	str r3, [r0]


.type	gpio_10baseT_nop, %function
.global gpio_10baseT_nop
gpio_10baseT_nop:
	nop

.type	gpio_10baseT_end, %function
.global gpio_10baseT_end
gpio_10baseT_end:
	bx lr

