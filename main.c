#include "stm32g0xx.h"

#include "mbclib.h"
#include "delay.h"

#include "uart.h"
#include "xprintf.h"


/* define one: */
#define MASTER_SPI_10BASET
/* #define GPIO_10BASET */


/* ethernet frame buffer */
u8 eth_frame[1530];


void init_clock_40MHz(void)
{
	// set latncy to one wait state because of higher clock
	FLASH->ACR &= ~ FLASH_ACR_LATENCY;
	FLASH->ACR |= FLASH_ACR_LATENCY_1;

	// switch PLL source to HSI16
	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLSRC;
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSI;

	// PLLOUT = (f*PLLN/PLLM)/PLLP = 16*5/1 /2 = 40MHz
	RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLN | RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLR );
	RCC->PLLCFGR |=	(5<<RCC_PLLCFGR_PLLN_Pos) | ((1-1)<<RCC_PLLCFGR_PLLM_Pos) | RCC_PLLCFGR_PLLR_0 | RCC_PLLCFGR_PLLREN ;

	// start pll
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY)) __asm__("nop");

	// switch to pll
	RCC->CFGR &= ~(RCC_CFGR_SW);
	RCC->CFGR |= RCC_CFGR_SW_1;
}

/* slight overclock (max specified == 64MHz) */
void init_clock_80MHz(void)
{
	// set latncy to one wait state because of higher clock
	FLASH->ACR &= ~ FLASH_ACR_LATENCY;
	FLASH->ACR |= FLASH_ACR_LATENCY_1;

	// switch PLL source to HSI16
	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLSRC;
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSI;

	// PLLOUT = (f*PLLN/PLLM)/PLLP = 16*10/1 /2 = 80MHz
	RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLN | RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLR );
	RCC->PLLCFGR |=	(10<<RCC_PLLCFGR_PLLN_Pos) | ((1-1)<<RCC_PLLCFGR_PLLM_Pos) | RCC_PLLCFGR_PLLR_0 | RCC_PLLCFGR_PLLREN ;

	// start pll
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY)) __asm__("nop");

	// switch to pll
	RCC->CFGR &= ~(RCC_CFGR_SW);
	RCC->CFGR |= RCC_CFGR_SW_1;
}


/* responsd to arp and udp packets.
 * see EXTI0_1_IRQHandler()
 */
void run_spi_10baseT(void)
{
	master_spi_10baseT_init();

	__enable_irq();

	/* send pulses spaced by 16ms */
	while (1) {

#if 0
		char response_payload[] = "padiiiiiiiiiiiing                 ";

		u32 byte_count = udp_ipv4_mac_eth(eth_frame, response_payload,
				sizeof(response_payload), 0x0a0001dc,
				1234, 1234);

		master_spi_10baseT_send_frame(&eth_frame, byte_count);
		delay(100000);
#endif

		master_spi_10baseT_NLP_pulse();
		delay(250000);
	}
}

/* runs in gpio mode.
 * it only sends some udp data (does not receive data)
 */
void run_gpio_spi_10baseT(void)
{
	gpio_10baseT_init();

	__enable_irq();


	char payload[] = "padiiiiiiiiiiiing                 ";
	u32 counter = 0;
	u32 byte_count;

	/* send some pulses spaced by 16ms */
	for (u32 i = 0; i < 100; ++i) {
		gpio_10baseT_NLP_pulse();
		delay(250000);
	}

	while (1) {
		xsprintf(&payload[17], "%d", counter++);

		/* get ethernet frame from udp data */
		byte_count = udp_ipv4_mac_eth(&eth_frame, &payload, sizeof(payload));

		gpio_10baseT_send_frame(&eth_frame, byte_count);
		delay(100000);

		gpio_10baseT_NLP_pulse();
		delay(250000);
	}
}

int main()
{
	__disable_irq();
	/* init_clock_40MHz(); */
	init_clock_80MHz();

	/* mco_clock_output(); */


	/* enable i/o */
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
	/* enable uart */
	RCC->APBENR2 |= RCC_APBENR2_USART1EN;
	/* enable syscfg for pin remapping */
	RCC->APBENR2 |= RCC_APBENR2_SYSCFGEN;
	/* enable spi1 */
	RCC->APBENR2 |= RCC_APBENR2_SPI1EN;
	/* enable tim2 */
	RCC->APBENR1 |= RCC_APBENR1_TIM2EN;


	/* wait for startup */
	delay(10);


	/*
	 * PA2 GPIO debug output
	 */
	GPIOA->BSRR = 0x10000<<2;
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED2;
	GPIOA->OSPEEDR |= 0x3 << GPIO_OSPEEDR_OSPEED2_Pos;
	GPIOA->MODER &= ~GPIO_MODER_MODE2;
	GPIOA->MODER |= GPIO_MODER_MODE2_0;


	/*
	 * GPIO pin remap
	 */
	/* remap PA11 to PA9 */
	SYSCFG->CFGR1 |= SYSCFG_CFGR1_PA11_RMP;
	/* remap PA12 to PA10 */
	SYSCFG->CFGR1 |= SYSCFG_CFGR1_PA12_RMP;
	GPIOA->MODER &= ~(GPIO_MODER_MODE10 | GPIO_MODER_MODE9);
	GPIOA->MODER |= GPIO_MODER_MODE10_1 | GPIO_MODER_MODE9_1;
	GPIOA->AFR[1] &= ~(GPIO_AFRH_AFSEL10 | GPIO_AFRH_AFSEL9);
	GPIOA->AFR[1] |= (1<<GPIO_AFRH_AFSEL10_Pos) | (1<<GPIO_AFRH_AFSEL9_Pos);


	/*
	 * USART1
	 *
	 * PA9  TX
	 * PA10 RX
	 */
	uart_init(USART1, 115200);
#ifdef DEBUG
	xfprintf(put_char, "uart working\n");
#endif



#if defined MASTER_SPI_10BASET
	run_spi_10baseT();
#elif defined GPIO_10BASET
	run_gpio_spi_10baseT();
#endif

	return 0;
}

