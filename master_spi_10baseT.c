#include "stm32g0xx.h"
#include "mbclib.h"
#include "uart.h"
#include "delay.h"


u8 tx_dma_buff[1530*4 + 12*4];
u8 rx_dma_buff[1530*4 + 12*4];
u8 rx_decoded[1530];


/* number of consecutive zeros that still count as 1 zero */
#define ZERO_LIMIT	2

/* number of consecutive ones that still count as 1 one */
#define ONE_LIMIT	3

/* number of consecutive bits that signal end of frame. */
#define EOF_LIMIT	7


#pragma GCC push_options
#pragma GCC optimize ("O3")

/*
 * master_spi_10baseT_NLP_pulse() is periodically called from main (it
 * should not be called form interrupt).
 * master_spi_10baseT_send_frame() is called from interrupt.
 *
 * it is possible that master_spi_10baseT_send_frame() interrupts
 * master_spi_10baseT_NLP_pulse(). In this case master_spi_10baseT_NLP_pulse()
 * would resend the previous frame instead of sending NLP (normal link pulse).
 *
 * to mitigate this race condition we are using nlp_interrupted_flag,
 * which cancels sending NLP if it was interrupted.
 * */
volatile u32 nlp_interrupted_flag = 0;

/*
 * manchester encoding.
 * each bit is represented by 4 bits
 * converts:
 * 1 to 0011
 * 0 to 1100
 */
static inline void manchester_1to4(u8 addr[4], u8 data_byte)
{
	/* lsb (least significant bit) first */
	/* we are expanding each bit into 4bits */
	for (u32 i = 0; i < 4; ++i) {

		addr[i] = 0;

		if ( data_byte & (1<<(2*i)) )
			/* send a 1 */
			addr[i] |= 0x0c;
		else
			/* send a 0 */
			addr[i] |= 0x03;

		if ( data_byte & (1<<(2*i + 1)) )
			/* send a 1 */
			addr[i] |= 0xc0;
		else
			/* send a 0 */
			addr[i] |= 0x30;
	}
}


/* returns:
 * 0 if ok
 * 1 if error (overflow)
 * 2 if end of frame (EOF)
 */
u32 goto_next_transition(u32 *curr_bit_val, u32 *curr_bit_count, u32 *in_byte_count, u32 *in_bit_count, u8 in_data[])
{
	*curr_bit_val = (in_data[*in_byte_count] >> *in_bit_count) & 1;
	*curr_bit_count = 1;

	while(*in_byte_count < sizeof(rx_dma_buff)) {

		(*in_bit_count)++;
		if (*in_bit_count == 8) {
			(*in_byte_count)++;
			*in_bit_count = 0;
		}

		u32 new_bit_val = (in_data[*in_byte_count] >> *in_bit_count) & 1;

		if (new_bit_val == *curr_bit_val) {
			/* no switch */
			(*curr_bit_count)++;

			if ((*curr_bit_count) > EOF_LIMIT) {
				/* end of frame */
				return 2;
			}
		} else {
			/* switch */
			return 0;
		}
	}

	/* buffer overflow */
	return 1;
}

u32 decode_frame(u8 in_data[], u8 out_data[], u32 *out_len)
{
	u32 in_bit_count = 0;
	u32 in_byte_count = 0;

	u32 zero_limit = ZERO_LIMIT;
	u32 one_limit = ONE_LIMIT;

	u32 curr_bit_val = 0;
	u32 curr_bit_count = zero_limit + one_limit + 1;


	/* skip to first high level (1) to
	 * prevent detecting start of preamble too soon */
	while(curr_bit_val != 1)
		if (goto_next_transition(&curr_bit_val, &curr_bit_count, &in_byte_count, &in_bit_count, in_data))
			return 1;

	/* skip to last rising edge of preamble */
	while(!((curr_bit_val == 0) && (curr_bit_count <= zero_limit)))
		if (goto_next_transition(&curr_bit_val, &curr_bit_count, &in_byte_count, &in_bit_count, in_data))
			return 1;
	/* we are currently here
	 *                        \
	 *                         \
	 *                          V
	 * -+     +-----+     +--+  +--+--+--+--+-
	 *  |     |     |     |  |  |  |  |  |  |
	 *  |     |     |     |  |  |  |  |  |  |
	 *  +-----+     +-----+  +--+  +--+--+--+-
	 *  0     1     0     1     1     d0    d1 ...
	 *          preamble           |   data
	 *
	 * (in_data[in_byte_count] >> in_bit_count) & 1 == 1
	 */

	/* xfprintf(put_char, "curr_bit_val: %d\n", curr_bit_val); */
	/* xfprintf(put_char, "curr_bit_count: %d\n", curr_bit_count); */
	/* xfprintf(put_char, "in_byte_count: %d\n", in_byte_count); */
	/* xfprintf(put_char, "in_bit_count: %d\n\n", in_bit_count); */
	/* xfprintf(put_char, "\n\n"); */

	u32 out_bit_count = 0;
	u32 out_byte_count = 0;

	/* prepare/clear zero-th byte */
	rx_decoded[0] = 0;

	while (1) {

		u32 ret = goto_next_transition(&curr_bit_val, &curr_bit_count, &in_byte_count, &in_bit_count, in_data);
		/* we are currently here (1) or here (2)
		 *                          (1) (2)
		 *                            \  \
		 *                             V  V
		 * -+     +-----+     +--+  +--+--+--+--+-
		 *  |     |     |     |  |  |  |  |  |  |
		 *  |     |     |     |  |  |  |  |  |  |
		 *  +-----+     +-----+  +--+  +--+--+--+-
		 *  0     1     0     1     1     d0    d1 ...
		 *          preamble           |   data
		 */

		if (ret == 1) {
			/* error detected */
			return 1;
		} else if (ret == 2) {
			/* detected end of frame */
			break;
		}

		if ((curr_bit_val == 0) && (curr_bit_count <= zero_limit) ||
		(curr_bit_val == 1) && (curr_bit_count <= one_limit)) {

			/* we are at (1). go to (2) */
			ret = goto_next_transition(&curr_bit_val, &curr_bit_count, &in_byte_count, &in_bit_count, in_data);

			if (ret == 1) {
				/* error detected */
				return 1;
			} else if (ret == 2) {
				/* detected end of frame */
				break;
			}
		}

		/* we are at (2) */
		u32 new_bit_val = (in_data[in_byte_count] >> in_bit_count) & 1;

		/* add bit to output buffer */
		rx_decoded[out_byte_count] = (new_bit_val << 7) | (rx_decoded[out_byte_count] >> 1);

		out_bit_count++;
		if (out_bit_count == 8) {
			out_byte_count++;
			out_bit_count = 0;
			/* prepare/clear next byte */
			rx_decoded[out_byte_count] = 0;
		}
	}

	*out_len = out_byte_count + 1;

	return 0;
}

u32 print_raw_frame(u8 rx_buff[])
{
	for (u16 i=0; ; ++i) {

		u8 byte = rx_buff[i];
		if (byte == 0xff)
			break;

		for (u8 j=0; j<8; ++j) {
			u8 bit = byte & 1;
			byte >>= 1;
			put_char('0' + bit);
		}
	}
	xfprintf(put_char, "\n\n");
}

void EXTI0_1_IRQHandler(void)
{
	GPIOA->BSRR = 0x1<<2;

	/* clear pending */
	EXTI->RPR1 = 0xff;
	EXTI->FPR1 = 0xff;


	/* first flush 4 buffered registers */
	u8 flush = *((volatile u8 *)&SPI1->DR);
	flush = *((volatile u8 *)&SPI1->DR);
	flush = *((volatile u8 *)&SPI1->DR);
	flush = *((volatile u8 *)&SPI1->DR);
	flush = *((volatile u8 *)&SPI1->DR);

	while ((SPI1->SR & SPI_SR_RXNE) == 0);
	u8 read = *((volatile u8 *)&SPI1->DR);


	/* check if we have a beginning of frame */
	if (read == 0xff) {
		/* it was just a Normal Link Pulse */
		GPIOA->BSRR = 0x10000<<2;
		return;
	}

	/* setup dma */
	DMA1_Channel2->CNDTR = sizeof(rx_dma_buff);

	/* start transfer */
	DMA1_Channel2->CCR |= DMA_CCR_EN;

	/* wait for end of frame sing */
	while (1) {
		read = *((volatile u8 *)&SPI1->DR);
		if (read == 0xff)
			break;
	};

	/* stop transfer */
	DMA1_Channel2->CCR &= ~DMA_CCR_EN;


	u32 len;
	if (decode_frame(rx_dma_buff, rx_decoded, &len)) {
		/* decoding error */
		/* __asm__("bkpt 42"); */
		GPIOA->BSRR = 0x10000<<2;
		return;
	}

	/* check if frame is long enough */
	if (len < 64) {
		/* __asm__("bkpt 42"); */
		GPIOA->BSRR = 0x10000<<2;
		return;
	}


#ifdef DEBUG
	xfprintf(put_char, "len: %d\n", len);
#endif

	process_eth_frame(rx_decoded, len);

	/* print_raw_frame(rx_dma_buff); */

#ifdef DEBUG
	xfprintf(put_char, "\n\n");
#endif

	GPIOA->BSRR = 0x10000<<2;
}


void master_spi_10baseT_init(void)
{
	/*
	 * TIM2 one pulse output
	 * PA1 CH2 af2
	 */
	GPIOA->BSRR = 0x10000<<1;
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED1;
	GPIOA->OSPEEDR |= 0x3 << GPIO_OSPEEDR_OSPEED1_Pos;
	GPIOA->MODER &= ~GPIO_MODER_MODE1;
	GPIOA->MODER |= GPIO_MODER_MODE1_1;
	GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL1;
	GPIOA->AFR[0] |= 2 << GPIO_AFRL_AFSEL1_Pos;


	/* upcount, single pulse mode */
	TIM2->CR1 = TIM_CR1_OPM;
	TIM2->CR2 = 0;

	/* set prescaler to 1; running at 80MHz */
	TIM2->PSC = 1-1;


	/* when timer is reset, value is zero and
	 * we want output to be low.
	 *
	 * in pwm mode 2 channel is INACTIVE as long as
	 * CNT < CCRx
	 * so we set CCR2 to 1 and adjust length of pulse
	 * with ARR.
	 * when timer is reset (CNT == 0) output is LOW.
	 */
	TIM2->CCMR1 = (7<<TIM_CCMR1_OC2M_Pos) | TIM_CCMR1_OC2PE;
	TIM2->CCR2 = 1;
	TIM2->ARR = 42;

	// enable channel output
	TIM2->CCER |= TIM_CCER_CC2E;

	// main output enable
	TIM2->BDTR |= TIM_BDTR_MOE;

	/* relaod values */
	TIM2->EGR = TIM_EGR_UG;

	// timer start
	TIM2->CR1 |= TIM_CR1_CEN;



	/*
	 * SPI1 MASTER mode:
	 * HARDWARE NSS/CS
	 *
	 * PA4 SPI_NSS af5
	 * PA5 SPI_SCK af5
	 * PA6 SPI_MISO af5
	 * PA7 SPI_MOSI af5
	 */
	GPIOA->MODER &= ~(GPIO_MODER_MODE7 | GPIO_MODER_MODE6 | GPIO_MODER_MODE5 | GPIO_MODER_MODE4);
	GPIOA->MODER |= GPIO_MODER_MODE7_1 | GPIO_MODER_MODE6_1 | GPIO_MODER_MODE5_1 | GPIO_MODER_MODE4_1;

	GPIOA->AFR[0] &= ~(GPIO_AFRL_AFSEL7 | GPIO_AFRL_AFSEL6 | GPIO_AFRL_AFSEL5 | GPIO_AFRL_AFSEL4);
	GPIOA->AFR[0] |= (0x0 << GPIO_AFRL_AFSEL7_Pos) | (0x0 << GPIO_AFRL_AFSEL6_Pos) |
			 (0x0 << GPIO_AFRL_AFSEL5_Pos) | (0x0 << GPIO_AFRL_AFSEL4_Pos);

	// set pins to high speed
	GPIOA->OSPEEDR &= ~(GPIO_OSPEEDR_OSPEED7 | GPIO_OSPEEDR_OSPEED6 | GPIO_OSPEEDR_OSPEED5 | GPIO_OSPEEDR_OSPEED4);
	GPIOA->OSPEEDR |= (0x3 << GPIO_OSPEEDR_OSPEED7_Pos) | (0x3 << GPIO_OSPEEDR_OSPEED6_Pos) |
			(0x3 << GPIO_OSPEEDR_OSPEED5_Pos) | (0x3 << GPIO_OSPEEDR_OSPEED4_Pos);

	/* hardware NSS/CS, LSB first, master, CPOL = 0, CPHA = 0 */
	SPI1->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_LSBFIRST;

	/* data size 8 bits,
	 * IMPORTANT: FIFO threshold at 8bit (instead of 16bit) */
	SPI1->CR2 = (0x7 << SPI_CR2_DS_Pos) | SPI_CR2_FRXTH;

	/* spi tx dma */
	SPI1->CR2 |= SPI_CR2_TXDMAEN;

	/* spi rx dma */
	SPI1->CR2 |= SPI_CR2_RXDMAEN;

	/* enable spi */
	SPI1->CR1 |= SPI_CR1_SPE;
	/* u32 irq = SPI1_IRQn; */
	/* NVIC->ISER[irq>>5] = (1<<(irq & 0x1f)); */



	/*
	 * FALLING EDGE INTERRUPT
	 * PA0
	 */

	/* set as input */
	GPIOA->MODER &= ~0x1;

	/* select portA */
	EXTI->EXTICR[0] &= ~0xff;

	/* falling edge interrupt for pin 0 */
	EXTI->FTSR1 = 0x1;

	/* unmask interrupt */
	EXTI->IMR1 |= 0x1;

	/* enable NVIC interrupt number */
	NVIC->ISER[0] = (1<<EXTI0_1_IRQn);




	/* clear tx_dma_buff */
	u8 *p = tx_dma_buff;
	while (p < tx_dma_buff + sizeof(tx_dma_buff))
		*p++ = 0;


	/* reference DMA1 so we can view it in debugger */
	DMA1;

	/* init DMA */
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	delay(10);


	/*
	 * setup tx dma
	 * we want to copy mem-to-periph (DIR = 1),
	 * 8bit to 8bit, increment source (memory),
	 * CIRCULAR NEVER ENDING MODE
	 */
	DMA1_Channel1->CCR = DMA_CCR_DIR | DMA_CCR_MINC | DMA_CCR_CIRC;

	DMA1_Channel1->CNDTR = sizeof(tx_dma_buff);
	DMA1_Channel1->CMAR = (u32) &tx_dma_buff;
	DMA1_Channel1->CPAR = (u32) (&SPI1->DR);

	/* setup DMAMUX channel0 to trigger DMA channel 1 for spi1_tx */
	DMAMUX1_Channel0->CCR = ((1-1)<<DMAMUX_CxCR_NBREQ_Pos) | 17;

	/* start transfer */
	DMA1_Channel1->CCR |= DMA_CCR_EN;

	tx_dma_buff[0] = 0x81;



	rx_dma_buff[0] = 0x42;
	rx_dma_buff[1] = 0x42;
	/*
	 * setup rx dma
	 * we want to copy periph-to-mem (DIR = 0),
	 * 8bit to 8bit, increment destination (memory),
	 */
	DMA1_Channel2->CCR = DMA_CCR_MINC; // | DMA_CCR_CIRC;

	DMA1_Channel2->CNDTR = sizeof(rx_dma_buff);
	DMA1_Channel2->CPAR = (u32) (&SPI1->DR);
	DMA1_Channel2->CMAR = (u32) &rx_dma_buff;

	/* setup DMAMUX channel1 to trigger DMA channel2 for spi1_rx */
	DMAMUX1_Channel1->CCR = ((2-1)<<DMAMUX_CxCR_NBREQ_Pos) | 16;
}


void master_spi_10baseT_send_frame(u8 *eth_frame, u32 byte_count)
{
	nlp_interrupted_flag = 1;

	if (byte_count < 72) {
		/* sorry your frame is too short */
		while (1);
	}

	if (byte_count > 1530) {
		/* sorry your frame is too long */
		/* also some buffer overflows probably occurred */
		while (1);
	}

	/* if TIM->CNT is not zero we are currently sending a frame,
	 * so we must wait */
	while(TIM2->CNT);

	u8 *p = tx_dma_buff;

	for (u32 i = 0; i < byte_count; ++i) {
		manchester_1to4(p, eth_frame[i]);
		p += 4;
	}

	/* append 1s as end of frame (EOF).
	 * duration is 8 bit times (800ns).
	 *
	 * it still works without this.
	 */
	*(p++) = 0xff;
	*(p++) = 0xff;
	*(p++) = 0xff;
	*(p++) = 0xff;
	byte_count += 1;


	/* set timer pulse length
	 * each byte has 8bits
	 * each bit requires 8timer ticks
	 * (timer runs at 80MHz, data runs at 10MHz)
	 */
	TIM2->ARR = 8*8*byte_count;

	while(1) {
		/* wait for new dma cycle */
		if (DMA1_Channel1->CNDTR == 1) {
			/* this is a carefully timed chain of nops */
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			TIM2->CR1 |= TIM_CR1_CEN;
			break;
		}
	}
}


void master_spi_10baseT_NLP_pulse()
{
	nlp_interrupted_flag = 0;

	/* if TIM->CNT is not zero we are currently sending a frame,
	 * so we must wait */
	while(TIM2->CNT);

	u8 *p = tx_dma_buff;

	*p = 0xff;

	TIM2->ARR = 8;

	while(1) {
		/* wait for new dma cycle */
		if (nlp_interrupted_flag) {
			/* frame was sent from interrupt
			 * while we were waiting */
			break;
		}
		if (DMA1_Channel1->CNDTR == 1) {
			/* this is a carefully timed chain of nops */
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			__asm__("nop");
			TIM2->CR1 |= TIM_CR1_CEN;
			break;
		}
	}
}

#pragma GCC pop_options

