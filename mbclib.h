#ifndef MBCLIB_H
#define MBCLIB_H

#include <stdint.h>

#define NULL ((void *)0)
#define LEN(arr) (sizeof arr / sizeof arr[0])
#define BIT(b) (1UL << b)

#if defined(STM32G0xx)
#include "stm32g0xx.h"
#elif defined(STM32G4xx)
#include "stm32g4xx.h"
#endif

typedef int8_t s8;
typedef uint8_t u8;

typedef int16_t s16;
typedef uint16_t u16;

typedef int32_t s32;
typedef uint32_t u32;

typedef int64_t s64;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

#endif /* MBCLIB_H */

