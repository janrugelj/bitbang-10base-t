
#include "uart.h"

void uart_init(USART_TypeDef *USART, u32 baud)
{
	USART->ISR |= USART_ISR_TXE_TXFNF;
	USART->CR1 |= USART_CR1_TE;
	USART->CR1 |= USART_CR1_RE;

	USART->BRR = F_CPU / baud;

	USART->CR1 |= USART_CR1_UE;
}

void uart_tx(USART_TypeDef *USART, char c)
{
	/* LF -> CRLF conversion */
	if (c == '\n')
		uart_tx(USART, '\r');

	while(!(USART->ISR & USART_ISR_TXE_TXFNF));

	USART->TDR = c;
}

/* put char on USART1 */
void put_char(char c)
{
	uart_tx(USART1, c);
}

