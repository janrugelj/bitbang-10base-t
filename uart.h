#ifndef USART_H
#define USART_H

#include "mbclib.h"

void uart_init(USART_TypeDef *USART, u32 baud);

void uart_tx(USART_TypeDef *USART, char c);

void put_char(char c);

#endif /* USART_H */

