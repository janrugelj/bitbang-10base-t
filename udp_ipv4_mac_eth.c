
#include "mbclib.h"
#include "uart.h"

extern struct uart_periph *uart1;

#define ETHTYPE_IPV4	0x0800
#define ETHTYPE_ARP	0x0806

#define IP_ICMP		0x01
#define IP_TCP		0x06
#define IP_UDP		0x11

extern u8 eth_frame[1530];

u8 response_udp_payload[1530];

struct ipv4_frame_t {
	u8 version_ihl;
	u8 dscp_ecn;
	u8 total_length[2];
	u8 identification[2];
	u8 flags_fragment_offset[2];
	u8 time_to_live;
	u8 protocol;
	u8 header_checksum[2];
	u8 source_ip_address[4];
	u8 destination_ip_address[4];
} __attribute((__packed__));

struct udp_frame_t {
	u8 source_port[2];
	u8 dest_port[2];
	u8 len[2];
	u8 checksum[2];
} __attribute((__packed__));

struct arp_frame_t {
	u8 htype[2];
	u8 ptype[2];
	u8 hlen;
	u8 plen;
	u8 oper[2];
	u8 sha[6];
	u8 spa[4];
	u8 tha[6];
	u8 tpa[4];
} __attribute((__packed__));


static u8 my_ip[4] = {10, 0, 1, 42};

/* mac address locally administered (byte0 bit1 is set) */
static u8 my_mac[6] = {0x2, 0x0, 0x0, 0x0, 0x0, 0x42};

/*
 * send udp over ipv4 over mac over ethernet
 * eth_frame must point to 1530byte buffer
 *
 * returns ethernet frame len
 */
u32 udp_ipv4_mac_eth(u8 *eth_frame, u8 *data, u32 data_len,
		u32 dest_ip_addr, u16 src_port, u16 dest_port)
{
	u8 *p = eth_frame;

	/* ethernet preamble */
	for (u32 i=0; i<7; ++i)
		*(p++) = 0x55;

	/* start of frame delimiter */
	*(p++) = 0xd5;

	/* receiver mac address */
	for (u32 i=0; i<6; ++i)
		*(p++) = 0xff;

	/* sender mac address */
	for (u32 i=0; i<6; ++i)
		*(p++) = my_mac[i];

	/* ethertype */
	*(p++) = 0x08;
	*(p++) = 0x00;



	/* ipv4 */
	struct ipv4_frame_t *ipv4 = p;
	p += sizeof(struct ipv4_frame_t);

	u16 ipv4_len = sizeof(struct ipv4_frame_t) +
		sizeof(struct udp_frame_t) + data_len;

	ipv4->version_ihl = 0x45;
	ipv4->dscp_ecn = 0x00;
	ipv4->total_length[0] = ipv4_len>>8;
	ipv4->total_length[1] = ipv4_len & 0xff;
	ipv4->identification[0] = 0x0;
	ipv4->identification[1] = 0x42;
	ipv4->flags_fragment_offset[0] = 0x40;
	ipv4->flags_fragment_offset[1] = 0x00;
	ipv4->time_to_live = 64;

	/* UDP protocol */
	ipv4->protocol = 17;

	/* set checksum to 0 for later calculation*/
	ipv4->header_checksum[0] = 0x00;
	ipv4->header_checksum[1] = 0x00;

	ipv4->source_ip_address[0] = my_ip[0];
	ipv4->source_ip_address[1] = my_ip[1];
	ipv4->source_ip_address[2] = my_ip[2];
	ipv4->source_ip_address[3] = my_ip[3];
	ipv4->destination_ip_address[0] = (dest_ip_addr >> 24) & 0xff;
	ipv4->destination_ip_address[1] = (dest_ip_addr >> 16) & 0xff;
	ipv4->destination_ip_address[2] = (dest_ip_addr >> 8) & 0xff;
	ipv4->destination_ip_address[3] = (dest_ip_addr >> 0) & 0xff;


	/*** calculate ipv4 header checksum ***/
	/* 32bit - for carry counting */
	u32 sum = 0;
	for (u32 i=0; i<10; ++i)
		sum += (( ((u8 *)ipv4)[2*i] << 8) | ((u8 *)ipv4)[2*i+1]);

	/* add carry bits */
	sum = (sum & 0xffff) + ((sum >> 16) & 0xffff);
	/* do it again in (rare) case of overflow */
	sum = (sum & 0xffff) + ((sum >> 16) & 0xffff);

	sum = ~sum;

	ipv4->header_checksum[0] = (sum >> 8) & 0xff;
	ipv4->header_checksum[1] = sum & 0xff;


	/* udp */
	struct udp_frame_t *udp = p;
	p += sizeof(struct udp_frame_t);

	u16 udp_len = sizeof(struct udp_frame_t) + data_len;

	udp->source_port[0] = (src_port >> 8) & 0xff;
	udp->source_port[1] = src_port & 0xff;
	udp->dest_port[0] = (dest_port >> 8) & 0xff;
	udp->dest_port[1] = dest_port & 0xff;
	udp->len[0] = (udp_len >> 8) & 0xff;
	udp->len[1] = udp_len & 0xff;
	udp->checksum[0] = 0x00;
	udp->checksum[1] = 0x00;

	/* udp payload */
	for (u32 i=0; i<data_len; ++i)
		*(p++) = *(data + i);



	/* ethernet crc32*/
	u8 *message = eth_frame + 8;

	u32 byte, crc, mask;
	crc = 0xFFFFFFFF;
	for (s32 i = 0; i < (p - eth_frame - 8); ++i) {
		byte = message[i];		// Get next byte.
		crc = crc ^ byte;
		for (s32 j = 7; j >= 0; --j) {	// Do eight times.
			mask = -(crc & 1);
			crc = (crc >> 1) ^ (0xEDB88320 & mask);
		}
	}
	crc =  ~crc;

	*(p++) = crc & 0xff;
	*(p++) = (crc>>8) & 0xff;
	*(p++) = (crc>>16) & 0xff;
	*(p++) = (crc>>24) & 0xff;

	return p - eth_frame;
}



u32 process_ipv4(u8 frame[], u32 len);
u32 respond_to_arp_request(u8 request_frame[], u32 len);
u32 respond_to_udp(u8 frame[], u32 len, u32 dest_ip_addr);

u32 process_eth_frame(u8 frame[], u32 len)
{
	u16 ethtype = (frame[12] << 8) | frame[13];
#ifdef DEBUG
	xfprintf(put_char, "ethtype: %x\n", ethtype);
#endif

	switch (ethtype) {
	case ETHTYPE_IPV4:
		process_ipv4(frame, len);
		break;
	case ETHTYPE_ARP:
		respond_to_arp_request(frame, len);
		break;
	}

}



u32 respond_to_arp_request(u8 request_frame[], u32 len)
{
	/* offset to beginning of arp frame */
	struct arp_frame_t *request = request_frame + 14;


	/* check if request was meant for our ip addr */
	for (u32 i=0; i<4; ++i)
		if (request->tpa[i] != my_ip[i])
			return;

#ifdef DEBUG
	xfprintf(put_char, "htype: %d\n", request->htype[0]);
	xfprintf(put_char, "htype: %d\n", request->htype[1]);
	xfprintf(put_char, "ptype: %d\n", request->ptype[0]);
	xfprintf(put_char, "ptype: %d\n", request->ptype[1]);
	xfprintf(put_char, "hlen: %d\n", request->hlen);
	xfprintf(put_char, "plen: %d\n", request->plen);
	xfprintf(put_char, "oper: %d\n", request->oper[0]);
	xfprintf(put_char, "oper: %d\n", request->oper[1]);
	xfprintf(put_char, "sha: %x\n", request->sha[0]);
	xfprintf(put_char, "sha: %x\n", request->sha[1]);
	xfprintf(put_char, "sha: %x\n", request->sha[2]);
	xfprintf(put_char, "sha: %x\n", request->sha[3]);
	xfprintf(put_char, "sha: %x\n", request->sha[4]);
	xfprintf(put_char, "sha: %x\n", request->sha[5]);
	xfprintf(put_char, "spa: %d\n", request->spa[0]);
	xfprintf(put_char, "spa: %d\n", request->spa[1]);
	xfprintf(put_char, "spa: %d\n", request->spa[2]);
	xfprintf(put_char, "spa: %d\n", request->spa[3]);
	xfprintf(put_char, "tha: %x\n", request->tha[0]);
	xfprintf(put_char, "tha: %x\n", request->tha[1]);
	xfprintf(put_char, "tha: %x\n", request->tha[2]);
	xfprintf(put_char, "tha: %x\n", request->tha[3]);
	xfprintf(put_char, "tha: %x\n", request->tha[4]);
	xfprintf(put_char, "tha: %x\n", request->tha[5]);
	xfprintf(put_char, "tpa: %d\n", request->tpa[0]);
	xfprintf(put_char, "tpa: %d\n", request->tpa[1]);
	xfprintf(put_char, "tpa: %d\n", request->tpa[2]);
	xfprintf(put_char, "tpa: %d\n", request->tpa[3]);
#endif

	u8 *p = eth_frame;

	/* ethernet preamble */
	for (u32 i=0; i<7; ++i)
		*(p++) = 0x55;

	/* start of frame delimiter */
	*(p++) = 0xd5;

	/* receiver mac address */
	for (u32 i=0; i<6; ++i)
		*(p++) = request->sha[i];

	/* sender mac address */
	for (u32 i=0; i<6; ++i)
		*(p++) = my_mac[i];

	/* ethertype */
	*(p++) = (ETHTYPE_ARP >> 8) & 0xff;
	*(p++) = ETHTYPE_ARP & 0xff;



	/* arp */
	struct arp_frame_t *response = p;
	p += sizeof(struct arp_frame_t);

	response->htype[0] = 0;
	response->htype[1] = 1;
	response->ptype[0] = 8;
	response->ptype[1] = 0;
	response->hlen = 6;
	response->plen = 4;
	response->oper[0] = 0;
	response->oper[1] = 2;
	response->sha[0] = my_mac[0];
	response->sha[1] = my_mac[1];
	response->sha[2] = my_mac[2];
	response->sha[3] = my_mac[3];
	response->sha[4] = my_mac[4];
	response->sha[5] = my_mac[5];
	response->spa[0] = my_ip[0];
	response->spa[1] = my_ip[1];
	response->spa[2] = my_ip[2];
	response->spa[3] = my_ip[3];
	response->tha[0] = request->sha[0];
	response->tha[1] = request->sha[1];
	response->tha[2] = request->sha[2];
	response->tha[3] = request->sha[3];
	response->tha[4] = request->sha[4];
	response->tha[5] = request->sha[5];
	response->tpa[0] = request->spa[0];
	response->tpa[1] = request->spa[1];
	response->tpa[2] = request->spa[2];
	response->tpa[3] = request->spa[3];

	/* add 18 bytes of padding because of minimal frame len */
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;
	*(p++) = 0;


	/* ethernet crc32*/
	u8 *message = eth_frame + 8;

	u32 byte, crc, mask;
	crc = 0xFFFFFFFF;
	for (s32 i = 0; i < (p - eth_frame - 8); ++i) {
		byte = message[i];		// Get next byte.
		crc = crc ^ byte;
		for (s32 j = 7; j >= 0; --j) {	// Do eight times.
			mask = -(crc & 1);
			crc = (crc >> 1) ^ (0xEDB88320 & mask);
		}
	}
	crc =  ~crc;

	*(p++) = crc & 0xff;
	*(p++) = (crc>>8) & 0xff;
	*(p++) = (crc>>16) & 0xff;
	*(p++) = (crc>>24) & 0xff;

	u32 eth_frame_len = p - eth_frame;
	master_spi_10baseT_send_frame(&eth_frame, eth_frame_len);
}


u32 process_ipv4(u8 frame[], u32 len)
{
	/* offset to beginning of ipv4 frame */
	struct ipv4_frame_t *ip_frame = frame + 14;


	/* check if request was meant for our ip addr */
	for (u32 i=0; i<4; ++i)
		if (ip_frame->destination_ip_address[i] != my_ip[i])
			return;

	u32 src_ip_addr = ip_frame->source_ip_address[0] << 24 |
		ip_frame->source_ip_address[1] << 16 |
		ip_frame->source_ip_address[2] << 8 |
		ip_frame->source_ip_address[3];

	switch (ip_frame->protocol) {
	case IP_ICMP:
		break;
	case IP_TCP:
		break;
	case IP_UDP:
		respond_to_udp(frame, len, src_ip_addr);
		break;
	}
}

u32 respond_to_udp(u8 frame[], u32 len, u32 dest_ip_addr)
{
	/* offset to beginning of upd frame */
	struct udp_frame_t *udp_frame = frame + 14 + sizeof(struct ipv4_frame_t);

#ifdef DEBUG
	xfprintf(put_char, "source_port: %x\n", udp_frame->source_port[0]);
	xfprintf(put_char, "source_port: %x\n", udp_frame->source_port[1]);
	xfprintf(put_char, "dest_port: %x\n", udp_frame->dest_port[0]);
	xfprintf(put_char, "dest_port: %x\n", udp_frame->dest_port[1]);
	xfprintf(put_char, "len: %x\n", udp_frame->len[0]);
	xfprintf(put_char, "len: %x\n", udp_frame->len[1]);
	xfprintf(put_char, "checksum: %x\n", udp_frame->checksum[0]);
	xfprintf(put_char, "checksum: %x\n", udp_frame->checksum[1]);
#endif

	u8 *payload = frame + 14 + sizeof(struct ipv4_frame_t) + sizeof(struct udp_frame_t);


	u32 udp_len = (udp_frame->len[0] << 8) | udp_frame->len[1];
	if (udp_len < sizeof(struct udp_frame_t))
		return 1;

	u32 payload_len = udp_len - sizeof(struct udp_frame_t);

#ifdef DEBUG
	for (u32 i=0; i<payload_len; ++i)
		xfprintf(put_char, "%c", payload[i]);
	xfprintf(put_char, "\n\n");
#endif


	u16 dest_port = (udp_frame->source_port[0] << 8) | udp_frame->source_port[1];
	u16 src_port = (udp_frame->dest_port[0] << 8) | udp_frame->dest_port[1];

	u32 response_payload_len = payload_len < sizeof(response_udp_payload) ?
		payload_len : sizeof(response_udp_payload);

	for (u32 i=0; i<sizeof(response_udp_payload) ; ++i)
		response_udp_payload[i] = 0;

	for (u32 i=0; i<response_payload_len; ++i) {
		char c = payload[i];
		if (('a' <= c) && (c <= 'z')) {
			response_udp_payload[i] = c + 'A' - 'a';
		} else if (('A' <= c) && (c <= 'Z')) {
			response_udp_payload[i] = c + 'a' - 'A';
		} else {
			response_udp_payload[i] = c;
		}
	}

	/* ethernet frame has minimal length */
	if (response_payload_len < 20)
		response_payload_len = 20;

	u32 byte_count = udp_ipv4_mac_eth(eth_frame, response_udp_payload,
			response_payload_len, dest_ip_addr,
			src_port, dest_port);
#ifdef DEBUG
	xfprintf(put_char, "byte_count: %d\n\n", byte_count);
#endif
	master_spi_10baseT_send_frame(&eth_frame, byte_count);
}

