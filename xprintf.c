#include "xprintf.h"
#include <stdarg.h>

/*
 * puts formated strings to various output devices
 */



/* output type (ot) - specifies type of output device */
struct output_type {
	enum {OT_FUNC, OT_BUFF} type;
	union {
		void (*func)(char);
		char **buff;
	};
};


/*
 * put a character on device defined by struct output_type
*/
static void xputc(struct output_type *ot, char c)
{
	switch (ot->type) {
	case OT_FUNC:
		ot->func(c);
		break;

	case OT_BUFF:
		*((*ot->buff)++) = c;
		break;
	}
}

/*
 * put a null-terminated string.
 */
static void xputs(struct output_type *ot, const char* str)
{
	if (str)
		while (*str)
			xputc(ot, *str++);
}


/*
 * gets lenght of string NOT including null byte
 */
u32 xstrlen(char *p)
{
	char *q = p;
	while (*p++);
	return p - q - 1;
}

/* reverse a string */
char *xstrrev(char *p)
{
	u32 len = xstrlen(p);
	for (u32 i = 0; i < len/2; ++i) {
		char tmp = *(p+i);
		*(p+i) = *(p+len-i-1);
		*(p+len-i-1) = tmp;
	}
	return p;
}

/* works for 2 <= radix <= 16 */
static void xnum_to_string(struct output_type *ot,
		u32 radix, u32 width, u32 is_signed, u32 num)
{
	is_signed = is_signed && ( ((s32)num) < 0 );

	if (is_signed)
		num = -((s32)num);

	/* 32bit binary + sign + NULL */
	char buf[34];
	u32 div = num;
	u32 mod;

	if (width > 32)
		width = 32;


	u32 i = 0;
	if (width == 0) {
		/* without leading zeros */
		while (i < 32) {
			mod = div % radix;
			div = div / radix;
			buf[i++] = mod < 10 ? '0' + mod : 'a' + (mod - 10);

			if (div == 0)
				break;
		}
	} else {
		/* fixed minimal width with leading zeros */
		while (i < 32) {
			mod = div % radix;
			div = div / radix;
			buf[i++] = mod < 10 ? '0' + mod : 'a' + (mod - 10);

			if ((div == 0) && (i > width))
				break;
		}
	}

	if (is_signed)
		buf[i++] = '-';

	buf[i] = '\0';
	xstrrev(buf);
	xputs(ot, buf);
}

/*
 * format string
 */
static void xvprintf(struct output_type *ot, const char* fmt, va_list arp)
{
	char c;
	u32 n;
	u32 width;

	if (!fmt)
		return;

	for(;;) {
		c = *fmt++;

		if (!c)
			break;

		if (c != '%') {
			xputc(ot, c);
			continue;
		}
		c = *fmt++;

		/* zero padded */
		width = 0;
		if (c == '0') {
			c = *fmt++;
			while ((c >= '0') && (c <= '9')) {
				width *= 10;
				width += c - '0';
				c = *fmt++;
			}
		}


		switch(c) {
		/* fun fact:
		 * if switch is in loop and there is nothing after switch,
		 * continue and break do the same thing.
		 */
		case 'S':
		case 's':
			xputs(ot, va_arg(arp, char*));
			continue;
		case 'C':
		case 'c':
			xputc(ot, (char)va_arg(arp, int));
			continue;
		case 'B':
		case 'b':
			n = va_arg(arp, int);
			xnum_to_string(ot, 2, width, 0, n);
			continue;
		case 'O':
		case 'o':
			n = va_arg(arp, int);
			xnum_to_string(ot, 8, width, 0, n);
			continue;
		case 'D':
		case 'd':
		case 'i':
			n = va_arg(arp, int);
			xnum_to_string(ot, 10, width, 1, n);
			continue;
		case 'U':
		case 'u':
			n = va_arg(arp, int);
			xnum_to_string(ot, 10, width, 0, n);
			continue;
		case 'X':
		case 'x':
		case 'p':
			n = va_arg(arp, int);
			xnum_to_string(ot, 16, width, 0, n);
			continue;
		default:
			xputc(ot, c);
			continue;
		}
	}
	return;
}

/*
 * put a formatted string to function (char by char)
 */
void xfprintf(void(*func)(char), const char* fmt, ...)
{
	struct output_type ot;
	ot.type = OT_FUNC;
	ot.func = func;

	va_list arp;
	va_start(arp, fmt);
	xvprintf(&ot, fmt, arp);
	va_end(arp);
}

/*
 * put a formatted string to the memory
 */
void xsprintf(char* buffp, const char* fmt, ...)
{
	struct output_type ot;
	ot.type = OT_BUFF;
	ot.buff = (void *)&buffp;

	va_list arp;
	va_start(arp, fmt);
	xvprintf(&ot, fmt, arp);
	va_end(arp);

	*buffp = '\0';
}

