#ifndef XPRINTF_H
#define XPRINTF_H

#include "mbclib.h"

void xfprintf (void(*func)(char), const char* fmt, ...);
void xsprintf (char* buffp, const char* fmt, ...);

#endif /* XPRINTF_H */

